package org.example;

import java.util.ArrayList;
import java.util.List;

public class Repository {
    private String nome; //nome del repository
    List<Commit> commits; //elenco commits in ordine cronologico

    //Costruttore
    public Repository(String nome){
        this.nome=nome;
        List<Commit> commits=new ArrayList<>();
        this.commits=commits;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Commit> getCommits(){
        return commits;
    }

    public void setCommits(List<Commit> commits){
        this.commits=commits;
    }

    //Metodo aggiungiCommit
    public void aggiungiCommit(Commit commit){
        commits.add(commit);
    }

    //Metodo ultimoCommit
    public Commit ultimoCommit() throws Exception {
        Commit commit=null;
        if(this.commits==null){
            //System.out.println("Non ci sono commit");
            throw new Exception("Non ci sono commit");
        }
        else{
            commit=commits.get((commits.size())-1);
        }
        return commit;
    }

    //Metodo numeroDiCommits
    public int numeroDiCommits(){
        int numero=0;
        if(commits==null){
            System.out.println("Non ci sono commit");
            numero=0;
        }
        else{
            numero=commits.size();
        }
        return numero;
    }
}
