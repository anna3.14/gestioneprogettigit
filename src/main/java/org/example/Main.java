package org.example;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Repository rep=new Repository("cartella");
        Commit commit1=new Commit("ger12","add metodo1");
        Commit commit2=new Commit("gtr12","add metodo2");

        //rep.aggiungiCommit(commit1);
        //rep.aggiungiCommit(commit2);
        System.out.println(rep.numeroDiCommits());
        System.out.println("--------------------------------------");
        try{
        System.out.println(rep.ultimoCommit().getMessaggio());
        }catch(Exception e){
            System.out.println("Non ci sono commits");
        }
    }
}